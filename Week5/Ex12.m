clc; 
clear;

%constants
mu = 0;
sigma = 1;
M = 100;
n = 10;
lambda = 0.1;

%sample
x_values = normrnd(mu,sigma,n,M);
y_values = poissrnd(lambda,n,M);

boot_x = bootstrp(1000,@mean,x_values);
boot_y = bootstrp(1000,@mean,y_values);
p_boot = zeros(M,1);
p_values =zeros(M,1);

for i = 1:M
    [~,p_values(i,1),~,~] = ttest2(x_values(:,i),y_values(:,i));
    [~,p_boot(i,1),~,~] = ttest2(boot_x(:,i),boot_y(:,i));
end

counter = 0;
counter_boot =0;

for i = 1:M
    if p_values(i,1) <= 0.05
        counter = counter+1;
    end
    if p_boot(i,1) <= 0.05
        counter_boot = counter_boot + 1;
    end
end

percentages = counter/100;
percentage_boot = counter_boot/100;