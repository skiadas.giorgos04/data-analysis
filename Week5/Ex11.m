clc 
clear 

%constants
mu = 0;
sigma = 1;
M =100;
n=10;
n2=12;
a = [0.05,0.025,0.1];
%sample  
x_values = normrnd(mu,sigma,n,M);
y_values = normrnd(mu,sigma,n2,M);

%bootstrap sample
x_boot = bootstrp(1000,@mean,x_values);
y_boot = bootstrp(1000,@mean,y_values);
p_boot = zeros(M,1);
p_values = zeros(M,1);
for i = 1:M
    [~,p_values(i,1),~,~] = ttest2(x_values(:,i),y_values(:,i));
    [~,p_boot(i,1),~,~] = ttest2(x_boot(:,i),y_boot(:,i));
end

%calculate rejection pecentage for bootstrap
counter_boot = 0;
counter = 0;
for i = 1:100    
   if p_values(i) <= a(1)
    counter = counter + 1;
   end
   if p_boot(i) <= a(1)
       counter_boot = counter_boot + 1;
   end
end
percentage = counter/100;
percentage_boot = counter_boot/100;


%Randomization test implementation

%Compute two means & Find the mean difference
mean_dif = mean(x_values,1) - mean(y_values,1);

%Combine
all_values = zeros(100,22);

all_values(1:10,1:100) = x_values;
all_values(11:22,1:100) = y_values;

%Shuffle
[rows, columns] = size(all_values);
shuf = randperm(columns);
temp = all_values;

temp(:,shuf) = all_values(:,:);

%Select new samples
x_shuf = temp(1:10,1:100);
y_shuf = temp(11:22,1:100);

%Compute two means & Find the mean difference for new random samples
mean_shuf_dif = mean(x_shuf,1)-mean(y_shuf,1);
counter_ones = 0;
for i = 1:100
    if abs(mean_shuf_dif(i)) >= mean_dif
        counter_ones  = counter_ones + 1;
    end
end

percentage_rand = counter_ones/100; 