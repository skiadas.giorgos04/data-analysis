clear
clc

%Question 1
M = 100;
y_param = [1,0.2];

%for second question change value of square_param to 1
square_param = 0;

%sample
x_values = normrnd(0,1,100,10);

%for first question choose y_param(1) for third question change to
%y_param(2)
y_values = normrnd(0,y_param(1),100,12);


if square_param == 1
    x_values = x_values.^2;
    y_values = y_values.^2;
end

ci = zeros(M,2);

lower_limit = (1000+1)*(0.05/2);
upper_limit = (1000+1 - lower_limit); 
limits = floor([lower_limit,upper_limit]);

for i = (1:M)
    
    [~,~,ci(i,:),~] = ttest2(x_values(i,:),y_values(i,:));
    
    bootstat1 = bootstrp(1000, @mean, x_values(i,:));
    bootstat2 = bootstrp(1000, @mean, y_values(i,:));
    
    dif = bootstat1 - bootstat2;
    dif = sort(dif);
    
end

ci_bootstrap = dif(limits);

figure(1)
histfit(dif,50)
    
    

