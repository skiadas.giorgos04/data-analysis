%Exercise 10 Chapter 3
clear 
clc
%constants
M = 100;
n =10;
sigma = 1;
mu = 0;

%for question beta change xsquared to 0
xsquared = 1;

mu_expected = [0,0.5,1,2];

%samples
x_values = normrnd(mu,sigma,M,n);

if xsquared == 0
    x_values = x_values.^2;
end

%for question beta change mu_expected to mu_expected(3), mu_expected(4)
%respectively

for i = 1:M
    [~,p_value(i,1),~,~] = ttest(x_values(i,:),mu_expected(1));
end

for i = 1:M
    [~,p_value2(i,1),~,~] = ttest(x_values(i,:),mu_expected(2));
end

%create bootstrap sample
boot_x = bootstrp(1000,@mean,x_values(1:100,:))';

%for question beta change mu_expected to mu_expected(3), mu_expected(4)
%respectively

for i = 1:100
    [~,p_value_boot(i,1),~,~] = ttest(boot_x(:,i),mu_expected(1));
end

for i = 1:100
    [~,p_value_boot2(i,1),~,~] = ttest(boot_x(:,i),mu_expected(2));
end

%initialize counters for percentage
counter = 0;
counter2 = 0;
counter_boot = 0;
counter_boot2 = 0;

a = [0.05,0.025,0.1];

for i = 1:100    
   if p_value(i) <= a(1)
       counter = counter + 1;
   end
   
   if p_value2(i) <= a(1)
       counter2 = counter2 + 1;
   end
   
   
   if p_value_boot(i) <= a(1)
       counter_boot = counter_boot + 1;
   end
   
   
   if p_value_boot2(i) <= a(1)
       counter_boot2 = counter_boot2 + 1;
   end
   
end

%rejection percentage
percentage = counter/100;
percentage2 = counter2/100;
percentage_boot = counter_boot/100;
percentage_boot2 = counter_boot2/100;
