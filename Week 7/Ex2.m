%Exercise 2 Chapter 5

clc 
clear 

%to check results for question 2 q_select = 2

%constants 
meu = [0,0];
sigma = [1,1];
ro = [0,0.5];
n = 20;
M = 1000;
 cov_matrix = [sigma(1)^2 ro(1)*sigma(1)*sigma(2);...
                  ro(1)*sigma(1)*sigma(2) sigma(2)^2];
q_select = 1;
counter = 0;
for k = 1:1000
   sample = mvnrnd(meu,cov_matrix,n);

    if q_select == 2
        sample = sample.^2;
    end

    cor_sample_new = zeros(M);
    cor = corrcoef(sample);
    cor_sample = cor(1,2);
    

    for i = 1:1000
        sample_x = sample(:,1);
        sample_y = sample(:,2);
        ran = randperm(n);
        sample_x = sample_x(ran);

        cor_new = corrcoef(sample_x,sample_y);
        cor_sample_new(i) = cor_new(1,2);
    end

    t_0 = cor_sample*sqrt((n-2)/(1-cor_sample^2));
    cor_sample_new = cor_sample_new(1:M,1);
    t = cor_sample_new.*sqrt((n-2)./(1-cor_sample_new.^2));
    t = sort(t);
    t_low = t(M*0.05/2);
    t_up = t(M*(1-0.05/2));


    
    if t_0 < t_up && t_0 > t_low
        counter = counter + 1;
    end

end

percentage = counter/(M*M);