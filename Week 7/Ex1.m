%Exercise 1 Chapter 5
clc 
clear

%to check results for questions 1 and 2 leave ex_select as is, 
%to check results for question 3 ex_select = 3
%to check results for question 4 ex_select = 4

%constants 
meu = [0,0];
sigma = [1,1];
ro = [0,0.5];
n = 20;
M = 1000;
cov_matrix = [sigma(1)^2 ro(1)*sigma(1)*sigma(2);...
              ro(1)*sigma(1)*sigma(2) sigma(2)^2];

ex_select = 1;          

if ex_select == 3
    n =200;
end

r = zeros(M);
for i = 1:M    
    
    %sample
    sample = mvnrnd(meu,cov_matrix,20);
    if ex_select == 4
        sample = sample.^2;
    end
    %correlation coef
    cor_coef = corrcoef(sample);
    r(i) = cor_coef(1,2);    
end

z = r(1:M,1);
z = 0.5*log((1+z)./(1-z));
z_citical = norminv(1-0.05/2);
lower_bound = z - z_citical.*(sqrt(1/(n-3)));
upper_bound = z + z_citical.*(sqrt(1/(n-3)));


figure(1)
histogram(lower_bound);
title("Lower Limits")
figure(2)
histogram(upper_bound);
title("Upper Limits")

counter = 0;
for i = 1:1000
    if ro(1)<upper_bound(i) && ro(1)>lower_bound(i)
        counter = counter + 1;
    end
end
percentage = counter/1000;


t = r.*sqrt((n-2)./(1-r.^2));
t_crit = tinv(1-0.05/2,n-2); 

t_lower = t-t_crit;
t_upper = t+t_crit;

counter = 0;
for i = 1:1000
    if ro(1)<t_upper(i) && ro(1)>t_lower(i)
        counter = counter + 1;
    end
end

percentage_t = counter/1000;

%for question 3 percentages drop to half 
%dor question 4 there is no major difference in our results
