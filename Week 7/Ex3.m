%Exercise 2 Chapter 5

clc 
clear 

data_temp = readmatrix('tempThess59_97.txt');
data_rain = readmatrix('rainThess59_97.txt');

coef = zeros(12);
t_0 = zeros(12);
percentage = zeros(12);
L =1000;

for i = 1:12
    month_temp = data_temp(:,i);
    month_rain = data_rain(:,i);
    coef = corrcoef(month_temp,month_rain);
    coef = coef(1,2);
    t_0(i) = coef*sqrt((39-2)/(1-coef^2));


    for j = 1:L
        ran = randperm(39);
        month_temp = month_temp(ran);

        coef = corrcoef(month_temp,month_rain);
        coef = coef(1,2);
        t(j) = coef*sqrt((39-2)/(1-coef^2));
    end
    
    t =sort(t);
    
    t_low = t(L*0.05/2);
    t_up = t(L*(1-0.05/2));
    
    counter = 0;
    if t_0(i) < t_up && t_0(i) > t_low
        counter = counter +1;
    end
    percentage(i) = counter/39;
end