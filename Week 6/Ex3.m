%Exercise 3 Chapter 4
clear
clc

%A
%s = sqrt((v*cosf)^2*sigma_v^2+(i*cosf)^2*sigma_i^2+(-v*i*sinf)^2*sigma_f^2)

%B

%constants
vf_cov = 0.5;

v_mean = 77.78;
v_std = 0.71;

i_mean = 1.21;
i_std = 0.071;

f_mean = 0.283;
f_std = 0.017;

v_sample = normrnd(v_mean,v_std,1000,1);
i_sample = normrnd(i_mean,i_std,1000,1);
f_sample = normrnd(f_mean,f_std,1000,1);

p_sample = v_sample.*i_sample.*cos(f_sample);

sigma_calculated = std(p_sample);
sigma_expected = sqrt(((i_mean*cos(f_mean))^2)*v_std^2+((v_mean*cos(f_mean))^2)*i_std^2+((-v_mean*i_mean*sin(f_mean))^2)*f_std^2);

%C
sigma_cov_expected = sqrt(((i_mean*cos(f_mean))^2)*v_std^2+((v_mean*cos(f_mean))^2)*i_std^2+...
    ((-v_mean*i_mean*sin(f_mean))^2)*f_std^2 + 2*(i_mean*cos(f_mean))*(-v_mean*i_mean*sin(f_mean))*...
    f_std*v_std*vf_cov);


cov_matrix = [i_std^2 0 0;...
    0 vf_cov^2 vf_cov*v_std*f_std;...
    0 vf_cov*v_std*f_std f_std^2]; 

 meu = [i_mean, v_mean, f_mean];
 
 r = mvnrnd(meu,cov_matrix,1000);
 
 p(:) = r(:,1).*r(:,2).*cos(r(:,3));
 sigma_cov_calculated = std(p);
 