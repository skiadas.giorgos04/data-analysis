%Exercise 1 Chapter 4

clear
clc

%constants, expressed in cm
h_1 = 100;
h_2 = [60,54,58,60,56];
e_expected = 0.76;
M = 1000;
n = 5;
e_calculated = sqrt(h_2/h_1);
sample = normrnd(58,2,n,M);

%Question 1
q1_h = ttest(e_calculated,e_expected);

%Question 2
sample_mean = mean(sample,1);
sample_std = std(sample,1);
e_sample = sqrt(sample/h_1);
mean_e = mean(e_sample,1);
std_e = std(e_sample,1);

q2_h = ttest(e_sample,e_expected);

%Question 3
h_3 = [80,100,90,120,95];
h_4 = [48,65,50,75,56];

e_q3 = sqrt(h_4./h_3);

q3_h = ttest(e_q3,e_expected);
if q3_h == 0
    display("Hypothesis for question 3 is accepeted");
end