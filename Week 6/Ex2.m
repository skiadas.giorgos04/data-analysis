%Exercise 2 Chapter 4
clc 
clear

%constants
s = 5;
length = 500;
width = 300;

%rectangle's surface type is surf = width * length
s_surf = sqrt((length*s)^2+(width*s)^2);

%  syms surf_plot length_plot width_plot
length_plot = 1:100;
width_plot = 1:100;
for i = 2:100
    for j = 1:i
        surf_plot(i,j) = sqrt((length_plot(i)*s).^2 +(width_plot(j)*s).^2);
    end
end

surf(length_plot,width_plot,surf_plot)

