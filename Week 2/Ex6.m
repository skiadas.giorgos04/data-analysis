N = 10000;
median_of_array = zeros(1,N);
counter = 1;
for repetitions = 1:N 
    random_values = unifrnd(0,1,5);
    disp(random_values);
    disp(median(random_values, 'all'));
    median_of_array(1,counter) = median(random_values, 'all');
    counter = counter+1;
    disp(counter);
    
end
disp(median_of_array);
histfit(median_of_array,1000,'normal');
