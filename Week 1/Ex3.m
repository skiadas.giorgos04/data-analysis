mu = [2,3];
sigma = [1 1.5; 1.5 3];
R = mvnrnd(mu , sigma, 1000);
fprintf('Covariance is:%f\n',var(R(:,1)+R(:,2)))
fprintf('Sum is:%f\n',var(R(:,1))+ var(R(:,2)));
if var(R(:,1)+R(:,2)) ~= var(R(:,1))+ var(R(:,2))
    disp('False')
end