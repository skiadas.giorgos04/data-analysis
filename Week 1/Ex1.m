Y_axis = zeros(1,1000);
n = 1000;
array_counter = 1;
for upper_limit = 1:n
    tails = 0 ;  
    for n = 1:upper_limit
        coin = rand();
        if coin > 0.5
            tails = tails + 1;
        end
    end
    disp(tails);
    ratio = tails/upper_limit;
    Y_axis(1 ,array_counter) = ratio;
    fprintf('Ratio is %f\n' , ratio);
    array_counter = array_counter + 1;
end
disp(Y_axis);
X_axis = 1:1000 ;
plot(X_axis, Y_axis);
