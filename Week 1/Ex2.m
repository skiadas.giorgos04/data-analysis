n = 1000;
Y_axis = zeros(1,1000);
exp_distrib = zeros(1,1000);
array_counter = 1;
for iterations = 1:1000
    random_number = rand();
    disp(random_number);
    %for λ = 1
    y = -log(1-random_number);
    Y_axis(1,array_counter) = y;
    array_counter = array_counter + 1;
end  
histfit(Y_axis,50,'exponential');