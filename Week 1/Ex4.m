data = zeros(1,n);
data_inverse = zeros(1,n);
n=100;
for iterations = 1:n
    array_counter = 1;
    for multitude = 1:iterations
        x = unifrnd(1,2,1,multitude); 
        x_inverse = 1./x;
        array_counter = array_counter+1;
    end
    data(1,array_counter) = mean(x_inverse);
    data_inverse(1,array_counter) = 1/mean(x); 
    fprintf('E[1/X] is :%f\n' ,mean(data));
    fprintf('1/E[X] is :%f\n' , 1/mean(x));
end
plot(data);
hold on;
plot(data_inverse);
hold off;