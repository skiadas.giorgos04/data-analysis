clc;

n=10;
m=100;
%create values from normal dist
values = normrnd(0,1,n,m);
chi_matrix = zeros(2,m);
for i = 1:m
    [~,~,chi,~] = vartest(values(:,i),var(values(:,i)))
    chi_matrix(:,i) = chi;
end
chi_matrix

chi = sqrt(chi_matrix);
boot_chi = bootci(100,@std,values)
figure()
histogram(chi,50)
hold on;
histogram(boot_chi,50,'EdgeAlpha',0.5)
hold off;


fprintf('\n~~~~~~~~~~~~~~~~~~\n');
fprintf('Second part\n');

%create y_squared values
y_values = values.^2;

for i = 1:m
    [~,~,chi,~] = vartest(y_values(:,i),var(y_values(:,i))) 
    y_chi_matrix(:,i) = chi;
end

y_chi = sqrt(y_chi_matrix);
y_boot_chi = bootci(100,@std,y_values);

figure()
histogram(y_chi,100)
hold on;
histogram(y_boot_chi,100,'EdgeAlpha',0.5)
hold off;