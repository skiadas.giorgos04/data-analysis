clc;
n=10;
values = normrnd(0,1,1,10);

%create bootstrap sample
bootstat = bootstrp(1000, @mean,values);
mean(bootstat);

figure()
histogram(bootstat)
hold on;
xline(mean(values),'-r','LineWidth',5)
hold off;

%bootstrap standard error
Boot_stnd_error = std(bootstat)/sqrt(length(bootstat))
values_stnd_error = std(values)/sqrt(length(values))
stdError = Boot_stnd_error-values_stnd_error


fprintf('\n~~~~~~~~~~~~~~~~~~\n');
fprintf('Second part\n');


%finding values for Y = e^X
y_values = exp(values);

%create new bootstrap sample
y_bootstat = bootstrp(1000, @mean,y_values);
mean(y_bootstat);

figure()
histogram(y_bootstat)
hold on;
xline(mean(y_values),'-r','LineWidth',5)
hold off;

%bootstrap standrad error
y_Boot_stnd_error = std(y_bootstat)/sqrt(length(y_bootstat))
y_values_stnd_error = std(y_values)/sqrt(length(y_values))
Y_std_error = y_Boot_stnd_error-y_values_stnd_error
