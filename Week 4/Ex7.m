clc
clear

n=10;
m=100;
%create values from normal dist
values = normrnd(0,1,n,m);

[~,~,chi,~] = ttest(values(:,:))
boot_chi = bootci(100,@mean,values)

figure()
histogram(chi,50)
hold on;
histogram(boot_chi,50,'EdgeAlpha',0.5)
hold off;


fprintf('\n~~~~~~~~~~~~~~~~~~\n');
fprintf('Second part\n');

%create y_squared values
y_values = values.^2;

[~,~,y_chi,~] = ttest(y_values(:,:));
%[~,~,y_boot_chi,~] = ttest(y_bootstat(:,:));
y_boot_chi = bootci(100,@mean,y_values)

figure()
histogram(y_chi,50)
hold on;
histogram(y_boot_chi,50,'EdgeAlpha',0.5)
hold off;