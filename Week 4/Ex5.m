clc;

%import data from txt file
data = readmatrix("eruption.txt");
waiting_time1989 = data(:,1);
waiting_time2006 = data(:,3);
duration = data(:,2);

%first q
[h_1989,~,chi_1989,~] = vartest(waiting_time1989,10);
std_chi_1989 = sqrt(chi_1989)
if std_chi_1989(1,1) < 10 && std_chi_1989(2,1) > 10
    disp("0");
else
    disp("1");
end

[h_2006,~,chi_2006,~] = vartest(waiting_time2006,10);
std_chi_2006 = sqrt(chi_2006)
if std_chi_2006(1,1) < 10 && std_chi_2006(2,1) > 10
    disp("0");
else
    disp("1");
end

[h,~,chi] = vartest(duration,1);
std_chi = sqrt(chi)
if std_chi(1,1) < 1 && std_chi(2,1) > 1
    disp("0");
else
    disp("1");
end

%second q
fprintf('\n~~~~~~~~~~~~~~~~~~\n');
fprintf('Second part\n');
[mean_h_1989,~,mean_chi_1989,~] = ttest(waiting_time1989,75)
[mean_h_2006,~,mean_chi_2006,~] = ttest(waiting_time2006,75)
[mean_h,~,mean_chi] = ttest(duration,2.5)

%third q
fprintf('\n~~~~~~~~~~~~~~~~~~\n');
fprintf('Third part\n');
[noraml_h_1989, normal_p_1989] = chi2gof(waiting_time1989)
[noraml_h_2006, normal_p_2006] = chi2gof(waiting_time2006)
[noraml_h, normal_p] = chi2gof(duration)

