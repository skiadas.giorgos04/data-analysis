%first part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('First part\n');
meu = 5;
n = 5 ;
numbers = random('Exponential',meu,1,5);
phat = mle(numbers, 'exp',meu);
disp(phat(1,1));
disp(mean(numbers));


%second part
fprintf("~~~~~~~~~~~~~~~~~~\n");
fprintf('Second part\n');
disp(expMeans(200,30,5));
figure();
disp(expMeans(100,50,4));
fprintf("~~~~~~~~~~~~~~~~~~\n");
function [globalMeans]=expMeans(m,n,meu)
     values = exprnd(meu,1,n,m);
     means = mean(values);
     histogram(means,10);
     globalMeans = mean(mean(values));
end