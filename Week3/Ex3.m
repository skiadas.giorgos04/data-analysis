%first part

fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('First part\n');

lifespan = exprnd(15, 5,1000); %1/λ = 15 month
real_lifespan = mean(lifespan);

[h,p,ci,stats] = ttest(lifespan);
ciMean(1,1) = mean(ci(1,:));
ciMean(2,1) = mean(ci(2,:));
disp(h);
disp(ci);
counter = 0;
for i = 1:1000
    if real_lifespan(1,i)>ciMean(1,1) && real_lifespan(1,i)<ciMean(2,1)
        counter = counter+1;
    end
end

percentage = (counter /1000)*100;
disp(percentage);

%second part

fprintf("~~~~~~~~~~~~~~~~~~\n");
fprintf('Second part\n');

lifespan2 = exprnd(15, 1000,100); %1/λ = 15 month
real_lifespan2 = mean(lifespan2);

[h2,p,ci2,stats] = ttest(lifespan2);
ciMean2(1,1) = mean(ci2(1,:));
ciMean2(2,1) = mean(ci2(2,:));
%disp(h2);
%disp(ci2);
counter2 = 0;
for i = 1:100
    if real_lifespan2(1,i)>ciMean2(1,1) && real_lifespan2(1,i)<ciMean2(2,1)
        counter2 = counter2+1;
    end
end

percentage2 = (counter2 /1000)*100;
disp(percentage2);