voltage = [41, 46, 47, 47, 48, 50, 50, 50, 50, 50, 50, 50,...
48, 50, 50, 50, 50, 50, 50, 50, 52, 52, 53, 55,...
50, 50, 50, 50, 52, 52, 53, 53, 53, 53, 53, 57,...
52 52, 53, 53, 53, 53, 53, 53, 54, 54, 55, 68];%number of values 12

arrayLength = length(voltage);

%first part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('First part\n');
[h,p,ci,stats] = vartest(voltage,var(voltage));
disp(ci);

%second part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('Second part\n');

stdSample = std(voltage);
disp(stdSample);


%third part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('Third part\n');

[h,p,ci2,stats] = ttest(voltage);
disp(ci2);


%Fourth part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('Fourth part\n');
ttest(voltage,52)

%fifth part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('Fifth part\n');

[h4,p4,stats4] = chi2gof(voltage);
if h4 == 1
    fprintf("Test returned TRUE\n");
else
    fprintf("Test returnd FALSE\n");
end

disp(p4);
fprintf('\n~~~~~~~~~~~~~~~~~~\n');
