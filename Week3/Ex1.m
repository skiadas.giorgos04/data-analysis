%first part
fprintf('~~~~~~~~~~~~~~~~~~\n');
fprintf('First part\n');
lambda = 5;
n = 5 ;
numbers = random('Poisson',lambda,1,5);
%disp(numbers);
phat = mle(numbers, 'Poiss',lambda);
disp(phat(1,1));
disp(mean(numbers));


%second part
fprintf("~~~~~~~~~~~~~~~~~~\n");
fprintf('Second part\n');
disp(poissonMeans(200,30,5,1));
figure();
disp(poissonMeans(100,50,4,1));
fprintf("~~~~~~~~~~~~~~~~~~\n");
function [globalMeans]=poissonMeans(m,n,lambda,i)
     values = poissrnd(lambda,1,n,m);
     means = mean(values);
     histogram(means,10);
     globalMeans = mean(mean(values));
end

